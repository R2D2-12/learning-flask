Learning Flask to write Application Task
======

1. Set up environment
2. Project Structure
3. Persist Data
4. Sign-up using forms
5. Logging In/Out
6. Authorization Roles / User Roles



7. Working with Places - add locations

    1. Setup
    ```
    pip install geocode
    ```
    2. Include leaflet.js for rendering map using cdn
    3. [Query places](http://localhost:5000/home)

8.  Place deliveries 

9.  Listen for API (webhooks)


This application would not be possible without the great flask tutorials out there.


Things to improve going forward
------

1. Instead of building basic authorization from scratch use [Flask-Login](http://flask.pocoo.org/extensions/)

2. Use [Semantic UI](https://semantic-ui.com/)